#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt4 import QtGui
import sys
from ventana import Ventana

#----------------FUNCIONES-----------------------
def main():
	app = QtGui.QApplication(sys.argv)
	ventana = Ventana()
	ventana.show()
	sys.exit(app.exec_())

if __name__ == '__main__':
	main()
	