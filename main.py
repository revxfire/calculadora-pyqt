#!/usr/bin/env python
# -*- coding: utf-8 -*-

#-------------IMPORTS---------------------------
from PyQt4 import QtCore,QtGui, uic
import sys

#-------------CONSTANTES-------------------------

NMAX = 16

#-----------------CLASES-------------------------
class Ventana(QtGui.QMainWindow):
	def __init__(self, parent = None):
		QtGui.QWidget.__init__(self, parent)
		uic.loadUi("ui/main2.ui", self)
		self.centrar()
		self.linePantalla.setMaxLength(NMAX)
		self.memoria = 0.0
		
		#BANDERAS
		self.clear = False
		self.bSuma = False
		self.BResta = False
		self.BMultiplicacion = False
		self.BDivision = False
		self.Com = True

	#KeyPress
	def keyPressEvent(self, event):
		key = event.key()
		if key == QtCore.Qt.Key_0:
			self.on_btn0_clicked()
		if key == QtCore.Qt.Key_1:
			self.on_btn1_clicked()
		if key == QtCore.Qt.Key_2:
			self.on_btn2_clicked()
		if key == QtCore.Qt.Key_3:
			self.on_btn3_clicked()
		if key == QtCore.Qt.Key_4:
			self.on_btn4_clicked()
		if key == QtCore.Qt.Key_5:
			self.on_btn5_clicked()
		if key == QtCore.Qt.Key_6:
			self.on_btn6_clicked()
		if key == QtCore.Qt.Key_7:
			self.on_btn7_clicked()
		if key == QtCore.Qt.Key_8:
			self.on_btn8_clicked()
		if key == QtCore.Qt.Key_9:
			self.on_btn9_clicked()
		if key == QtCore.Qt.Key_Escape:
			self.on_btnAC_clicked()
		if key == QtCore.Qt.Key_Plus:
			self.on_btnSumar_clicked()
		if key == QtCore.Qt.Key_Minus:
			self.on_btnRestar_clicked()
		if key == QtCore.Qt.Key_Comma:
			self.on_btnComa_clicked()
		if key == QtCore.Qt.Key_Asterisk:
			self.on_btnMultiplicar_clicked()
		if key == QtCore.Qt.Key_Slash:
			self.on_btnDividir_clicked()
		if key == QtCore.Qt.Key_Enter:  #Intro del NumLock
			self.on_btnIgual_clicked()
		if key == QtCore.Qt.Key_Return:  #Intro teclado Normal
			self.on_btnIgual_clicked()
		if key == QtCore.Qt.Key_Period:  #Intro teclado Normal
			self.on_btnComa_clicked()

	def centrar(self):
		screen = QtGui.QDesktopWidget().screenGeometry()
		size = self.geometry()
		self.move((screen.width() - size.width())/2, (screen.height() - size.height())/2)

	@QtCore.pyqtSlot()
	def on_btn1_clicked(self):
		if self.clear:
			self.borrar()
			self.clear = False

		self.verificarCero()
		self.linePantalla.insert("1")

	@QtCore.pyqtSlot()
	def on_btn2_clicked(self):
		if self.clear:
			self.borrar()
			self.clear = False
		self.verificarCero()
		self.linePantalla.insert("2")

	@QtCore.pyqtSlot()
	def on_btn3_clicked(self):
		if self.clear:
			self.borrar()
			self.clear = False
		self.verificarCero()
		self.linePantalla.insert("3")

	@QtCore.pyqtSlot()
	def on_btn4_clicked(self):
		if self.clear:
			self.borrar()
			self.clear = False
		self.verificarCero()
		self.linePantalla.insert("4")

	@QtCore.pyqtSlot()
	def on_btn5_clicked(self):
		if self.clear:
			self.borrar()
			self.clear = False
		self.verificarCero()
		self.linePantalla.insert("5")

	@QtCore.pyqtSlot()
	def on_btn6_clicked(self):
		if self.clear:
			self.borrar()
			self.clear = False
		self.verificarCero()
		self.linePantalla.insert("6")

	@QtCore.pyqtSlot()
	def on_btn7_clicked(self):
		if self.clear:
			self.borrar()
			self.clear = False
		self.verificarCero()
		self.linePantalla.insert("7")

	@QtCore.pyqtSlot()
	def on_btn8_clicked(self):
		if self.clear:
			self.borrar()
			self.clear = False
		self.verificarCero()
		self.linePantalla.insert("8")

	@QtCore.pyqtSlot()
	def on_btn9_clicked(self):
		if self.clear:
			self.borrar()
			self.clear = False
		self.verificarCero()
		self.linePantalla.insert("9")

	@QtCore.pyqtSlot()
	def on_btn0_clicked(self):
		self.verificarCero()
		self.linePantalla.insert("0")

	@QtCore.pyqtSlot()
	def on_btnSumar_clicked(self):
		self.clear = True
		self.bSuma = True
		self.memoria += float(self.linePantalla.text())
		self.linePantalla.setText(str(self.memoria))

	@QtCore.pyqtSlot()
	def on_btnRestar_clicked(self):
		self.clear = True
		self.BResta = True
		if self.memoria == 0:
			self.memoria = float(self.linePantalla.text())
		else:	
			self.memoria -= float(self.linePantalla.text())
		
		self.linePantalla.setText(str(self.memoria))

	@QtCore.pyqtSlot()
	def on_btnMultiplicar_clicked(self):
		self.clear = True
		self.BMultiplicacion = True
		if self.memoria == 0:
			self.memoria = float(self.linePantalla.text())
		else:
			self.memoria *= float(self.linePantalla.text())
		self.linePantalla.setText(str(self.memoria))

	@QtCore.pyqtSlot()
	def on_btnDividir_clicked(self):
		self.clear = True
		self.BDivision = True
		if self.memoria == 0:
			self.memoria = float(self.linePantalla.text())
		else:
			self.memoria /= float(self.linePantalla.text())

		self.linePantalla.setText(str(self.memoria))

	@QtCore.pyqtSlot()
	def on_btnIgual_clicked(self):
		if self.bSuma:
			self.memoria += float(self.linePantalla.text())
			self.bSuma = False

		elif self.BResta:
			self.memoria -= float(self.linePantalla.text())
			self.BResta = False

		elif self.BMultiplicacion:
			self.memoria *= float(self.linePantalla.text())
			self.BMultiplicacion = False

		elif self.BDivision:
			self.memoria /= float(self.linePantalla.text())
			self.BMultiplicacion = False

		self.linePantalla.setText(str(self.memoria))
		self.memoria = 0  #Reset Memory
		self.Com = True  #Reset Points

	@QtCore.pyqtSlot()
	def on_btnComa_clicked(self):
		if self.Com:
			self.linePantalla.insert(".")
			self.Com = False #No Se pueden Usar Mas Puntos!

	@QtCore.pyqtSlot()
	def on_btnAC_clicked(self):
		self.linePantalla.setText("0")
		self.memoria = 0
		self.clear = False
		self.bSuma = False
		self.BResta = False
		self.BMultiplicacion = False
		self.BDivision = False
		self.Com = True


	def verificarCero(self):
		if float(self.linePantalla.text()) == 0.0:
			self.linePantalla.setText("")
	
	def borrar(self):
		self.linePantalla.setText("0")

	def acerca(self):
		reply = QtGui.QMessageBox.question(self, 'Acerca de:',
            """<p><b>Creado por Victor Villalobos</b> </p>\n
            	<p><b>2013-09, Curso PyQT4 Online</b></p>
            	<center><p><a href="http://www.fenux.com.ar">WebSite </a> </p>
            	<p> Licencia GPL V2 </p></center>""" )

        



#----------------FUNCIONES-----------------------
def main():
	app = QtGui.QApplication(sys.argv)
	ventana = Ventana()
	ventana.show()
	sys.exit(app.exec_())

if __name__ == '__main__':
	main()
	